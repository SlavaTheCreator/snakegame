// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Snake.h"
#include "PlayerPawnBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
    if(bIsHead){
        auto Snake = Cast<ASnake>(Interactor);
        if(IsValid(Snake)){
            Snake->AddSnakeElement();
            ActivateEffect(Snake);
            //Snake->ChangeBodyColor(FVector(200, 0, 0));
            AddPoints();
            this->Destroy();
        }
    }
}

void AFood::AddPoints(int points){
    auto PPawn = Cast<APlayerPawnBase>(GetWorld()->GetFirstPlayerController()->GetPawn());
    if(IsValid(PPawn)){
        PPawn->Points += points;
    }
}

void AFood::ActivateEffect_Implementation(ASnake* Snake)
{
}

