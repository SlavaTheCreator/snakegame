// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    ElementSize_ = 100.f;
    MovementSpeed = 0.25f;
    LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
    SetActorTickInterval(MovementSpeed);
	AddSnakeElement(3);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    Move();
}

void ASnake::AddSnakeElement(int ElementsNum){
	for (int i = 0; i < ElementsNum; ++i) {
        FVector NewLocation(SnakeElements.Num() * ElementSize_, 0, 0);
        if(SnakeElements.Num()){
            ASnakeElementBase* LastElem = SnakeElements[SnakeElements.Num()-1];
            FVector LastElemPos = LastElem->GetActorLocation();

            NewLocation = CalcNewPosition(LastElemPos);
        }
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
        NewSnakeElem->SnakeOwner = this;
        int32 idx = SnakeElements.Add(NewSnakeElem);
        if(idx == 0){
            NewSnakeElem->SetFirstElementType();
        }
	}
}

void ASnake::Move()
{
    FVector MovementVector(FVector::ZeroVector);

    switch (LastMoveDirection)
    {
        case EMovementDirection::UP:
            MovementVector.X += ElementSize_;
            break;
        case EMovementDirection::DOWN :
            MovementVector.X -= ElementSize_;
            break;
        case EMovementDirection::LEFT :
            MovementVector.Y += ElementSize_;
            break;
        case EMovementDirection::RIGHT :
            MovementVector.Y -= ElementSize_;
            break;
    }

    //AddActorWorldOffset(MovementVector);
    SnakeElements[0]->ToggleCollision();

    for (int i = SnakeElements.Num() - 1; i > 0; i--) {
        auto CurrentElement = SnakeElements[i];
        auto PrevElement = SnakeElements[i - 1];
        FVector PrevLocation = PrevElement->GetActorLocation();
        CurrentElement->SetActorLocation(PrevLocation);
    }

    SnakeElements[0]->AddActorWorldOffset(MovementVector);
    SnakeElements[0]->ToggleCollision();
}

void ASnake::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other){
    if(IsValid(OverlappedElement)){
        int32 ElemIndex;
        SnakeElements.Find(OverlappedElement, ElemIndex);
        bool bIsFirst = ElemIndex == 0;
        IInteractable* InteractableInterface = Cast<IInteractable>(Other);
        if(InteractableInterface){
            InteractableInterface->Interact(this, bIsFirst);
        }
    }
}

void ASnake::ChangeBodyColor(FVector NewRGB)
{
    for (int i = SnakeElements.Num() - 1; i > 0; i--) {
        SnakeElements[i]->ChangeColor(NewRGB);
    }
}

FVector ASnake::CalcNewPosition(FVector LastElemVector){
    switch (LastMoveDirection)
    {
        case EMovementDirection::UP:
            LastElemVector.X -= ElementSize_;
            break;
        case EMovementDirection::DOWN :
            LastElemVector.X += ElementSize_;
            break;
        case EMovementDirection::LEFT :
            LastElemVector.Y -= ElementSize_;
            break;
        case EMovementDirection::RIGHT :
            LastElemVector.Y += ElementSize_;
            break;
    }

    return LastElemVector;
}